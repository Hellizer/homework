package ru.Drobyshev.Task1.Work3;
// Написать программу для вывода на экран таблицы умножения до введенного пользователем порога.
public class HomeWork3 {
    public static void main(String[] args) {
        //так как в задании не уточняется по какому из множителей применять порог - он будет применен к обоим множителям и построен квадрат
        int limit =0;

        try {
            if (args.length>0) {
                limit = Integer.parseInt(args[0]);
            }
        } catch (NumberFormatException ex) {
            System.out.println("параметр не переводится в integer");
        }

        if (limit != 0) {
            printTable(limit);
        } else {
            System.out.println("требуется вести корректный параметр");
        }
    }

    private static void printTable(int limit) {
        for (int i=1;i<=limit;i++){
            for (int j=1; j<=limit;j++){
               int result =i*j;
                System.out.printf(" %4d ",result);
            }
            System.out.println("");
        }
    }
}
