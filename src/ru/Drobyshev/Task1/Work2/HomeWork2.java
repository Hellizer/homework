package ru.Drobyshev.Task1.Work2;

import java.util.Arrays;

//Написать программу для поиска минимального из 4 чисел.
public class HomeWork2 {

    public static void main(String[] args) {

        int[] intArray = {3,6,2,5};

        //простейший вариант
        Arrays.sort(intArray);
        System.out.println(intArray[0]);


        int[] intArray2 = {3,6,2,5};

        //вариант с нахождением минимального значения через цикл
        int minimal = intArray2[0];
        for (int i=1; i<intArray2.length-1;i++){
            if (minimal>intArray2[i]){
                minimal=intArray2[i];
            }
        }
        System.out.println(minimal);
    }
}