package ru.Drobyshev.Task1.Work1;
//программу, которая считает стоимость бензина (на вход программе передается кол-во литров,
// на выходе печатается стоимость). Пример: стоимость литра бензина 43 рубля. На вход подается 50,
// на выходе должно быть 2150 руб.
public class Homework1 {
    /** цена за литр**/
    static final int FUEL_PRICE=43;

    public static void main(String[] args) {
        if (args.length>0 ){
            /* считаем что литры у нас бывают только целыми*/
            System.out.println(String.format("Результат: %d",Integer.parseInt(args[0])*FUEL_PRICE));
        } else {
            System.out.println("Нужно ввести параметр");
        }
        /*более простая запись*/
        String cost = args.length > 0? String.format("Результат: %d",Integer.parseInt(args[0])*FUEL_PRICE) : "нужно ввести параметр";
        System.out.println(cost);
    }
}
