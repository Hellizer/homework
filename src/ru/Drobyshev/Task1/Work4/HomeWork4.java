package ru.Drobyshev.Task1.Work4;
//Написать программу, которая выводит факториал для N чисел.

//будем считать только при вводе положительных чисел

public class HomeWork4 {
    public static void main(String[] args) {
        int limit=20;
        try {
            if (args.length>0) {
                limit = Integer.parseInt(args[0]);
            }
        } catch (NumberFormatException ex) {
            System.out.println("параметр не переводится в integer");
        }

        if (limit >0 ) {
            System.out.printf("результат %,d",factorial(limit));
        } else {
            System.out.printf("требуется вести корректный параметр (целое положительное число 1..%,d)",20);
        }
    }

    public static long factorial(long n){
        if (n==1) return 1;
        return n*factorial(n-1);
    }
}
