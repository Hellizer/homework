package ru.Drobyshev.Task2.Work1.drinks;

public interface Drink {
    double getPrice();
    String getTitle();
}