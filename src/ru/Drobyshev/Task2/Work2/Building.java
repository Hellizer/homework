package ru.Drobyshev.Task2.Work2;

public class Building {
    private Appartaments[] _appartaments;
    private final int _appartamentsCount;
    private final int _buildingNo;

    public Building(int number, int _appartamentsCount) {
        this._appartamentsCount = _appartamentsCount;
        _appartaments = new Appartaments[_appartamentsCount];
        _buildingNo=number;
        for (int i = 0; i < _appartamentsCount ; i++) {
            _appartaments[i] = new Appartaments(i+1);
        }
    }

    public int getAppartamentsCount() {
        return _appartamentsCount;
    }

    public Appartaments getAppartamentsInfo(int num) throws Exception {
        if (num < _appartamentsCount) {
            return _appartaments[num-1];
        }else {
            throw new Exception("В доме нет такой квартиры");
        }
    }

    private int calcEmptyApprts(){
        int count=0;
        for (Appartaments ap: _appartaments ) {
            if (ap.isEempty()){
                count++;
            }
        }
        return  count;
    }

    public String getSatistic(){
        StringBuilder sb = new StringBuilder("Дом номер: ");
        sb.append(_buildingNo + "\n");
        sb.append("Всего квартир: "  + _appartamentsCount + "\n");
        int emptyapprts = calcEmptyApprts();
        sb.append("Свободно квартир: " + emptyapprts);
        if (emptyapprts < _appartamentsCount) {
            sb.append("\nСданы квартиры: \n");
            for (Appartaments aprts: _appartaments) {
                if (!aprts.isEempty()){
                    sb.append( aprts.getInfo() + "\n");
                }
            }
        }
        return sb.toString();
    }
}
