package ru.Drobyshev.Task2.Work2;

import java.util.Date;

public class Appartaments {
    private int _appartamentNo;
    private int _monthCost;
    private Person _renter;
    private boolean _empty;
    private Date _rentFromDate;

    public void setRentValue(int _monthCost) {
        this._monthCost = _monthCost;

    }

    public void setRenter(Person renter, int value) {
        this._renter = renter;
        setRentValue(value);
        _empty = false;
        _rentFromDate = new Date();
    }

    public int getAppartamentNo() {
        return _appartamentNo;
    }

    public int getMonthCost() {
        return _monthCost;
    }

    public Person getRenter() {
        return _renter;
    }

    public boolean isEempty(){
        return _empty;
    }

    public Appartaments(int _appartamentNo) {
        this._appartamentNo = _appartamentNo;
        _empty = true;
    }

    public String getInfo() {
        String info;
        info = _empty? "Свободна": "Квартира № " + _appartamentNo + ", Сдается с "+ _rentFromDate + ", Жилец: " + _renter.getName() ;
        return info;
    }

}
