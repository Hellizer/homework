package ru.Drobyshev.Task2.Work2;

public class Person {
    private String _firstName;
    private String _secondName;

    public Person(String _firstName, String _secondName) {
        this._firstName = _firstName;
        this._secondName = _secondName;
    }

    public String getName() {
        return _firstName + " " + _secondName;
    }


    @Override
    public String toString() {
        return "Фамилия, Имя: " + _secondName + " " +  _secondName;
    }
}
