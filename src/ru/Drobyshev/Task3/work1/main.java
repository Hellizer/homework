package ru.Drobyshev.Task3.work1;

import java.util.ArrayList;

public class main {
    public static void main(String[] args) {

        System.out.println("Hello World");

        String phrase = "Hello World";

        ArrayList<String> stringArr = new ArrayList<>();
        stringArr.add(phrase);
        stringArr.add(null);
        try {
            System.out.println(stringArr.get(1).getBytes());
        }catch (Exception e){
            System.out.println(e);
        }
        //простая ошибка
        System.out.println(phrase.charAt(0));
        try{
            System.out.println(phrase.charAt(110));
        }catch (Exception e){
            System.out.println(e);
        }

        //ошибка ArrayIndexOutOfBoundsException
        byte[] arr = phrase.getBytes();
        try{
            System.out.println(arr[11]);
        }catch (Exception e){
            System.out.println(e);
        }

        //бросим свою ошибку
        try {
            throw new Exception("просто вызвали ошибку");
        } catch (Exception e){
            System.out.println(e);
        }
    }
}
