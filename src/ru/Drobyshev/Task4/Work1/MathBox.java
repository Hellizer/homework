package ru.Drobyshev.Task4.Work1;



import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.TreeSet;

public class MathBox {
    private HashSet<Integer> innerArray;

    public MathBox(int[] arr) throws Exception{
        innerArray = new HashSet<>();
        for (int i=0; i<arr.length; i++){
            addElement(arr[i]);
        }

    }
// дабы убрать повторяемы код
    private void addElement (int element) throws Exception{
        if (!innerArray.add(element)){
            throw new Exception("Ошибка: нельзя добавить повторяющиеся элементы");
        }
    }

    public int summator (){
        //решение простое
        int summ1 =0,summ2 = 0;
        for (Integer element: innerArray) {
            summ1 += element;
        }
        //решение на основе расчета hashCode от Set и Integer.hashCode
        summ2=innerArray.hashCode();
        //на случай если предидущее утверждение окажется не верным
        return (summ1==summ2)? summ2: summ1;
    }

    public void splitter (int delimiter) throws Exception{
        if (delimiter==0){
            throw new Exception("Ошибка: делитель равен нулю");
        }

        HashSet<Integer> tempSet = (HashSet<Integer>) innerArray.clone();
        innerArray.clear();

        for (Integer element: tempSet) {
            addElement(element / delimiter);
            innerArray.remove(element) ;
        }
    }


    public void changeElement(int element, int newValue) throws Exception{
        if (innerArray.remove(element)){
            addElement(newValue);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Array = {" );
//конструкция чтобы расставиьт запятые между элементами
// есть проблема - элементы будут отсортированы странным образом
        Iterator iter = innerArray.iterator();
        while (iter.hasNext()){
            sb.append(iter.next());
            if (iter.hasNext()){
                sb.append(", ");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MathBox mathBox = (MathBox) o;
        return equals(mathBox);
    }

    private boolean equals (MathBox m2){
        //первые две проверки дабы избежать долгой проверки по элементам если заведомо разные массивы
        if (innerArray.size()!=m2.innerArray.size()) return false;
        if (innerArray.hashCode()!=m2.innerArray.hashCode()) return false;
        return innerArray.containsAll(m2.innerArray);
    }

    @Override
    public int hashCode() {
        return innerArray.hashCode()+ innerArray.size();
    }


}
