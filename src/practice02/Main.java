package practice02;

import java.util.Arrays;

import static practice02.Reverse.put;

public class Main {
    public static void main(String[] args) {
        Reverse rev = new Reverse(10);
        put(1,0);
        put(2,1);
        put(3,2);
        put(4,3);
        put(5,4);
        put(6,5);
        put(7,6);
        put(8,7);
        put(9,8);
        put(10,9);

        rev.display();
        System.out.println(Arrays.toString(rev.reverse()));
    }
}
