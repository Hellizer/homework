package practice02;

import java.util.Stack;

public class Reverse {
    static Integer[] intArray;
    int size;
    Stack stack = new Stack();

    public Reverse(int size) {
        intArray = new Integer[size];
        this.size = size;
    }

    public int[] reverse() {
        int[] reverseArray = new int[size];
        for (int i = 0; i <size ; i++) {
            stack.push(intArray[i]);
        }
        for (int i = 0; i <size ; i++) {
           reverseArray[i]= (int) stack.pop();
        }
        return reverseArray;
    }

    public static void put(int item, int index) {
        intArray[index] = item;
    }

    public void display() {
        for (int i = 0; i < size; i++) {
            System.out.printf("%d ", intArray[i]);
        }
    }
}
