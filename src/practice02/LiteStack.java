package practice02;

public class LiteStack {
    static int[] array;
    static int size;
    static int pos=0;

    {
        array = new int[100];
    }

    public  static void push(int item) {
        array[pos] = item;
        pos++;
    }

    public static int pop (){
        return array[pos--];
    }


}
