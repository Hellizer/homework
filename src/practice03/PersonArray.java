package practice03;

import java.util.Arrays;
import java.util.Comparator;

public class PersonArray {
    private static Person[] array;

    public PersonArray(Person[] array) {
        this.array = array;
    }

    public static void main(String[] args) {
        Person person1 = new Person("Ann",10);
        Person person2 = new Person("Masha",17);
        Person person3 = new Person("Bisha",13);
        Person person4 = new Person("Vlad",23);
        Person person5 = new Person("Vlad",20);

        array = new Person[]{person1,person2,person3,person4,person5};
        System.out.println(Arrays.toString(array));
        //Arrays.sort(array);
        //Arrays.sort(array,new PersonComparator());
        Arrays.sort(array, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        System.out.println(Arrays.toString(array));
    }

}
