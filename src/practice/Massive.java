package practice;

public class Massive {
    int[][] intArray;
    int size;

    public Massive(int size) {
        intArray = new int[size][size];
        this.size=size;
    }

    public void toRight(){
        int b;
        for (int i = 0; i < size ; i++) {
            for (int j = size-1; j > 0; j--) {
                intArray[i][j] = intArray[i][j-1];
            }
            //intArray[i][size-1] = 0;
        }
    }

    public void toLeft(){
        for (int i = 0; i < size ; i++) {
            for (int j = 0; j < size-1; j++) {
                intArray[i][j] = intArray[i][j+1];
            }
            intArray[i][size-1] = 0;
        }
    }

    public void display(){
        for (int i = 0; i < size ; i++) {
            for (int j = 0; j < size; j++) {
                System.out.printf(" %d ", intArray[i][j]);
            }
            System.out.println();
        }
    }

    public void fillArray(){
        for (int i = 0; i < size ; i++) {
            for (int j = 0; j < size; j++) {
                //intArray[i][j]= (int) ((Math.random()+1)*100);
                intArray[i][j]= j;
            }
        }
    }
}
